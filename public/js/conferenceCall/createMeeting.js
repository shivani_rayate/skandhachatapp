
$(document).ready(function () {

    // create Meeting form
    $("#create-meeting-form").submit(function (event) {
        event.preventDefault();

        // Strip spaces and make string lowercase
        const meetingName = $('#meetingName').val().trim().toLowerCase().replace(/\s/g, '')

        saveMeeting(meetingName)
    })
})

function saveMeeting(meetingName) {

    var date = new Date();
    var params = {
        meetingName: `${meetingName}_${date.getHours()}-${date.getMinutes()}-${date.getSeconds()}`
    }

    $.post('/v1/video/saveMeeting',
        params,
        function (data, status) {
            if (data.status === 200) {
                console.log(data.message);

                var meetingCode = data.data.meetingCode;
                sessionStorage.setItem('flag', btoa('isHostTrue'));
                window.location.href = `/v1/video/conferenceCall?meetingCode=${meetingCode}`;

            } else {
                console.log(data.message);
            }
        })
}