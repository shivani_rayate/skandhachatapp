var token = "";
var streamId = "";
var playOnly = "";
if (playOnly == "null") {
    playOnly = false;
}

var turn_off_camera_button = document.getElementById("turn_off_camera_button");
var turn_on_camera_button = document.getElementById("turn_on_camera_button");
var mute_mic_button = document.getElementById("mute_mic_button");
var unmute_mic_button = document.getElementById("unmute_mic_button");

var roomOfStream = new Array();
var streamsList = new Array();

var publishStreamId;
var isDataChannelOpen = false;
var isMicMuted = true;
var isCameraOff = true;

var autoRepublishEnabled = true;
var autoRepublishIntervalJob = null;

function checkAndRepublishIfRequired() {
    var iceState = webRTCAdaptor.signallingState(publishStreamId);
    if (iceState == null || iceState == "failed" || iceState == "disconnected") {
        console.log("Publish has stopped and will try to re-publish");
        webRTCAdaptor.stop(publishStreamId);
        webRTCAdaptor.closePeerConnection(publishStreamId);
        webRTCAdaptor.closeWebSocket();
        initialise_webRTCAdaptor(true, autoRepublishEnabled);
    }
    else {
        console.log("\n iceState >>  ", iceState);
    }
}

function turnMicrophone() {
    if (isMicMuted) {
        unmuteLocalMic();
    } else {
        muteLocalMic();
    }
}


function turnCamera() {
    if (isCameraOff) {
        turnOnLocalCamera();
    } else {
        turnOffLocalCamera();
    }
}


function turnOffLocalCamera() {
    webRTCAdaptor.turnOffLocalCamera();
    isCameraOff = true;
    $('.cameraToggle').toggleClass("active");
    sendNotificationEvent("CAM_TURNED_OFF");
}

function turnOnLocalCamera() {
    webRTCAdaptor.turnOnLocalCamera();
    isCameraOff = false;
    $('.cameraToggle').toggleClass("active");
    sendNotificationEvent("CAM_TURNED_ON");
}

function muteLocalMic() {
    webRTCAdaptor.muteLocalMic();
    isMicMuted = true;
    $('.microphoneToggle').toggleClass("active");
    sendNotificationEvent("MIC_MUTED");
}

function unmuteLocalMic() {
    webRTCAdaptor.unmuteLocalMic();
    isMicMuted = false;
    $('.microphoneToggle').toggleClass("active");
    sendNotificationEvent("MIC_UNMUTED");
}


function sendNotificationEvent(eventType, username = '', message = '', remoteStreamId = '') {
    if (isDataChannelOpen) {

        var notEvent = {
            streamId: publishStreamId,
            eventType: eventType,
            username: username,
            message: message,
            remoteStreamId: remoteStreamId,
        };

        webRTCAdaptor.sendData(publishStreamId, JSON.stringify(notEvent));
    } else {
        console.log("Could not send the notification because data channel is not open.");
    }
}

function handleNotificationEvent(obj) {
    console.log("Received data : ", obj.event.data);
    var notificationEvent = JSON.parse(obj.event.data);
    if (notificationEvent != null && typeof (notificationEvent) == "object") {
        var eventStreamId = notificationEvent.streamId;
        var eventTyp = notificationEvent.eventType;
        var eventUsername = notificationEvent.username;
        var eventMessage = notificationEvent.message;
        var eventRemoteStreamId = notificationEvent.remoteStreamId;

        if (eventTyp == "CAM_TURNED_OFF") {
            console.log("Camera turned off for : ", eventStreamId);
            $(`#controls_${eventStreamId}`).find(`.remote-camera-btn`).addClass('active');
        } else if (eventTyp == "CAM_TURNED_ON") {
            console.log("Camera turned on for : ", eventStreamId);
            $(`#controls_${eventStreamId}`).find(`.remote-camera-btn`).removeClass('active');
        } else if (eventTyp == "MIC_MUTED") {
            console.log("Microphone muted for : ", eventStreamId);
            $(`#controls_${eventStreamId}`).find(`.remote-microphone-btn`).addClass('active');
        } else if (eventTyp == "MIC_UNMUTED") {
            console.log("Microphone unmuted for : ", eventStreamId);
            $(`#controls_${eventStreamId}`).find(`.remote-microphone-btn`).removeClass('active');
        } else if (eventTyp == 'MESSAGE') {
            appendMessage(`<span class="text-info">${eventUsername} : </span><span class="text-white">${eventMessage}</span>`);
        } else if (eventTyp == 'HAND_UP') {
            console.log('Hand raised for: ', eventStreamId);
            $(`#hand${eventStreamId}`).css('display', 'block');
        } else if (eventTyp == 'HAND_DOWN') {
            console.log('Hand down for: ', eventStreamId);
            $(`#hand${eventStreamId}`).css('display', 'none');
        } else if (eventTyp == 'SCREEN_SHARE_STARTED') {
            $(`.host-video-player`).css({ 'transform': 'none', '-webkit-transform': 'none' });
        } else if (eventTyp == 'SCREEN_SHARE_STOPPED') {
            $(`.host-video-player`).css({ 'transform': 'scaleX(-1)', '-webkit-transform': 'scaleX(-1)' });
        } else if (eventTyp == 'LEAVED_FROM_ROOM') {
            toastr.options = { "positionClass": "toast-bottom-left" };
            toastr.info(`${eventUsername} has left the meeting.`);
        } else if (eventTyp == 'JOINED_THE_ROOM') {
            toastr.options = { "positionClass": "toast-bottom-left" };
            toastr.info(`${eventUsername} joined.`);
        } else if (eventTyp == 'REMOTE_MIC_MUTED') {
            if (publishStreamId === eventRemoteStreamId) {
                muteLocalMic();
            }
        } else if (eventTyp == 'REMOTE_CAM_TURNED_OFF') {
            if (publishStreamId === eventRemoteStreamId) {
                turnOffLocalCamera();
            }
        }
    }
}


function joinRoom() {
    webRTCAdaptor.joinRoom(meetingCode, streamId);
}

async function leaveRoom() {

    sendNotificationEvent('LEAVED_FROM_ROOM', username);
    webRTCAdaptor.leaveFromRoom(meetingCode);
    unmuteLocalMic();
    turnOnLocalCamera();

    for (var node in document.getElementById("players").childNodes) {
        if (node.tagName == 'DIV' && node.id != "localVideo") {
            document.getElementById("players").removeChild(node);
        }
    }

    // redirect to create/join meeting page
    window.location.href = `/v1/video/createMeeting`
}

function publish(streamName, token) {
    publishStreamId = streamName;
    webRTCAdaptor.publish(streamName, token);
}

function streamInformation(obj) {
    webRTCAdaptor.play(obj.streamId, token, meetingCode);
}


function playVideo(obj) {
    getMeeting().then((getInfoRes) => {
        //console.log(`\n getMeeting response >>>>  ${JSON.stringify(getInfoRes)}`);
        var hostEmail = getInfoRes.host.email;
        var participants = getInfoRes.participants;
        var hostObj = participants.find((o) => o.email === hostEmail);
        var participant = participants.find((o) => o.streamId === obj.streamId);

        console.log(`participant ${JSON.stringify(participant)}`);
        console.log(`hostObj ${JSON.stringify(hostObj)}`);

        let isParticipantHost;
        if (hostObj.streamId === obj.streamId) {
            isParticipantHost = true;
        } else {
            isParticipantHost = false;
        }

        var room = roomOfStream[obj.streamId];
        console.log("new stream available with id: "
            + obj.streamId + "on the room:" + room);

        var video = document.getElementById("remoteVideo" + obj.streamId);

        if (video == null) {
            createRemoteVideo(participant, isParticipantHost);
            video = document.getElementById("remoteVideo" + obj.streamId);
        }

        video.srcObject = obj.stream;
    });

}


function createRemoteVideo(participant, isParticipantHost) {
    if (isParticipantHost) {
        $('#hostVideoContainer').empty();
        var player = document.createElement(`div`);
        player.className = `host-video-container`;
        player.id = `player${participant.streamId}`;
        player.innerHTML = `<video class="host-video-player" id="remoteVideo${participant.streamId}" data-hostStreamId="${participant.streamId}" autoplay></video><div class="host-name">${participant.name}</div>`;
        document.getElementById("hostVideoContainer").appendChild(player);
    } else {
        var player = document.createElement(`div`);
        player.className = `remote-video-container`;
        player.id = `player${participant.streamId}`;
        player.innerHTML = `<video class="remote-video-player" id="remoteVideo${participant.streamId}" autoplay></video><div class="controls" id="controls_${participant.streamId}" data-streamId="${participant.streamId}"> <i class="mdi mdi-microphone-off remote-microphone-btn"></i> <i class="mdi mdi-video-off remote-camera-btn"></i> </div><div class="over-lay-text">${participant.name}</div><i class="mdi mdi-hand" id='hand${participant.streamId}'></i>`;
        document.getElementById("players").appendChild(player);
    }
}



function removeRemoteVideo(streamId) {
    var video = document.getElementById("remoteVideo" + streamId);
    if (video != null) {
        var player = document.getElementById("player" + streamId);
        video.srcObject = null;
        document.getElementById("players").removeChild(player);
    }
}

function startAnimation() {

    $("#broadcastingInfo")
        .fadeIn(
            800,
            function () {
                $("#broadcastingInfo")
                    .fadeOut(
                        800,
                        function () {
                            var state = webRTCAdaptor
                                .signallingState(publishStreamId);
                            if (state != null
                                && state != "closed") {
                                var iceState = webRTCAdaptor
                                    .iceConnectionState(publishStreamId);
                                if (iceState != null
                                    && iceState != "failed"
                                    && iceState != "disconnected") {
                                    startAnimation();
                                }
                            }
                        });
            });

}

var pc_config = {
    'iceServers': [{
        'urls': 'stun:stun.l.google.com:19302'
    }]
};

var sdpConstraints = {
    OfferToReceiveAudio: false,
    OfferToReceiveVideo: false
};

var mediaConstraints = {
    video: { width: 1024, height: 576 }, // 1280 * 720
    //video: { aspectRatio: 16 / 9 }, // not working in firefox
    audio: true
};


// var websocketURL = "ws://ant-media.skandha.tv:5443/WebRTCAppEE/websocket"
var websocketURL = 'ws://3.7.232.114:5080/WebRTCAppEE/websocket';

if (window.location.protocol.startsWith('https')) {
    var websocketURL = 'wss://ant-media.skandha.tv:5443/WebRTCAppEE/websocket';
    //var websocketURL = "wss://13.126.29.174:5080/WebRTCAppEE/websocket"
}

var webRTCAdaptor = null;
function initialise_webRTCAdaptor(publishImmediately, autoRepublishEnabled) {
    webRTCAdaptor = new WebRTCAdaptor(
        {
            websocket_url: websocketURL,
            mediaConstraints: mediaConstraints,
            peerconnection_config: pc_config,
            sdp_constraints: sdpConstraints,
            localVideoId: "localVideo",
            debug: true,
            callback: function (info, obj) {
                if (info == "initialized") {
                    console.log("initialized");

                    if (publishImmediately) {
                        webRTCAdaptor.publish(streamId, token)
                    }

                    //After receiving initialized notification, you can call joinRoom method.
                    joinRoom();

                } else if (info == "joinedTheRoom") {
                    var room = obj.ATTR_ROOM_NAME;
                    roomOfStream[obj.streamId] = room;
                    console.log("joined the room: "
                        + roomOfStream[obj.streamId]);
                    console.log(obj)
                    publish(obj.streamId, token);

                    // webRTCAdaptor.getRoomInfo(meetingCode, obj.streamId);

                    var streams = obj.streams;
                    if (obj.streams != null) {
                        obj.streams.forEach(function (item) {
                            webRTCAdaptor.play(item, token, meetingCode);
                        });
                    }

                    addParticipant(obj.streamId);

                    if (isHost) {
                        $(`.host-video-player`).attr('data-hostStreamId', obj.streamId);
                    }

                } else if (info === 'streamJoined') {
                    console.debug('stream joined with id ' + obj.streamId);
                    webRTCAdaptor.play(obj.streamId, token, meetingCode);
                }
                else if (info == "newStreamAvailable") {
                    playVideo(obj);
                } else if (info === 'streamLeaved') {
                    console.debug('stream leaved with id ' + obj.streamId);
                    removeRemoteVideo(obj.streamId);
                }
                else if (info == "publish_started") {
                    //stream is being published
                    console.debug("publish started to room: "
                        + roomOfStream[obj.streamId]);
                    //startAnimation();
                    if (autoRepublishEnabled && autoRepublishIntervalJob == null) {
                        autoRepublishIntervalJob = setInterval(() => {
                            checkAndRepublishIfRequired(meetingCode);
                        }, 3000);
                    }
                } else if (info == "publish_finished") {
                    //stream is being finished
                    console.debug("publish finished");

                    // if (streamsList != null) {
                    //     streamsList.forEach(function (item) {
                    //         removeRemoteVideo(item);
                    //     });
                    // }

                    // we need to reset streams list
                    //streamsList = new Array();
                } else if (info == "leavedFromRoom") {
                    var room = obj.ATTR_ROOM_NAME;
                    console.debug("leaved from the room:" + room);

                } else if (info === 'browser_screen_share_supported') {
                    console.log('\n browser screen share supported');
                } else if (info === 'screen_share_stopped') {
                    console.log('screen share stopped');

                    $(`.host-video-player`).css({ 'transform': 'scaleX(-1)', '-webkit-transform': 'scaleX(-1)' });
                    $(`.host-video-player`).removeClass('force-hidden');
                    $(`.screen-share-active-screen`).addClass('force-hidden');
                    if (isCameraOff) {
                        turnOnLocalCamera();
                    }
                    sendNotificationEvent('SCREEN_SHARE_STOPPED');

                    var screenShareBtnId = document.getElementById("screen-share-btn");
                    screenShareBtnId.innerHTML = "Present now";
                    screenShareFlag = false;

                } else if (info == "closed") {
                    //console.log("Connection closed");
                    if (typeof obj != "undefined") {
                        console.log("Connecton closed: "
                            + JSON.stringify(obj));
                    }
                } else if (info == "play_finished") {
                    console.log("play_finished");
                    var video = document.getElementById("remoteVideo"
                        + obj.streamId);
                    if (video != null) {
                        video.srcObject = null;
                    }
                    webRTCAdaptor.getStreamInfo(obj.streamId);
                } else if (info == 'ice_connection_state_changed') {
                    console.log('\n iceConnectionState Changed: ', JSON.stringify(obj));
                } else if (info == "updated_stats") {
                    console.log("\n Average outgoing bitrate " + obj.averageOutgoingBitrate + " kbits/sec"
                        + " Current outgoing bitrate: " + obj.currentOutgoingBitrate + " kbits/sec");
                } else if (info == "refreshConnection") {
                    checkAndRepublishIfRequired(meetingCode);
                }
                else if (info == "streamInformation") {
                    streamInformation(obj);
                }
                // else if (info == "roomInformation") {
                //     var tempRoomStreamList = Array();
                //     // Check stream is in room
                //     // PS: Old room list mean streams doesn't have own stream ID
                //     if (streamsList != null) {
                //         for (i = 0; i < streamsList.length; i++) {
                //             var oldStreamListItem = streamsList[i];

                //             var oldRoomItemIndex = streamsList.indexOf(oldStreamListItem);
                //             var newRoomItemIndex = obj.streams.indexOf(oldStreamListItem);

                //             // If streams item is in obj.streams, it's 
                //             if (obj.streams.includes(oldStreamListItem)) {
                //                 if (newRoomItemIndex > -1) {
                //                     obj.streams.splice(newRoomItemIndex, 1);
                //                 }
                //                 tempRoomStreamList.push(oldStreamListItem);
                //             }
                //             else {
                //                 removeRemoteVideo(oldStreamListItem);
                //             }
                //         }
                //     }

                //     //Play new streams in list
                //     if (obj.streams != null) {
                //         obj.streams.forEach(function (item) {
                //             tempRoomStreamList.push(item);
                //             console.log("Stream joined with ID: " + item);
                //             webRTCAdaptor.play(item, token,
                //                 meetingCode);
                //         });
                //     }
                //     streamsList = tempRoomStreamList;
                // }
                else if (info == "data_channel_opened") {
                    console.log("Data Channel open for stream id", obj);
                    isDataChannelOpen = true;
                } else if (info == "data_channel_closed") {
                    console.log("Data Channel closed for stream id", obj);
                    isDataChannelOpen = false;
                } else if (info == "data_received") {
                    handleNotificationEvent(obj);
                }
            },
            callbackError: function (error, message) {
                //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError

                console.log("error callback: " + JSON.stringify(error));
                var errorMessage = JSON.stringify(error);
                if (typeof message != "undefined") {
                    errorMessage = message;
                }
                var errorMessage = JSON.stringify(error);
                if (error.indexOf("NotFoundError") != -1) {
                    errorMessage = "Camera or Mic are not found or not allowed in your device.";
                } else if (error.indexOf("NotReadableError") != -1
                    || error.indexOf("TrackStartError") != -1) {
                    errorMessage = "Camera or Mic is being used by some other process that does not not allow these devices to be read.";
                } else if (error.indexOf("OverconstrainedError") != -1
                    || error.indexOf("ConstraintNotSatisfiedError") != -1) {
                    errorMessage = "There is no device found that fits your video and audio constraints. You may change video and audio constraints."
                } else if (error.indexOf("NotAllowedError") != -1
                    || error.indexOf("PermissionDeniedError") != -1) {
                    errorMessage = "You are not allowed to access camera and mic.";
                } else if (error.indexOf("TypeError") != -1) {
                    errorMessage = "Video/Audio is required.";
                } else if (error.indexOf("UnsecureContext") != -1) {
                    errorMessage = "Fatal Error: Browser cannot access camera and mic because of unsecure context. Please install SSL and access via https";
                } else if (error.indexOf("WebSocketNotSupported") != -1) {
                    errorMessage = "Fatal Error: WebSocket not supported in this browser";
                } else if (error.indexOf("no_stream_exist") != -1) {
                    //TODO: removeRemoteVideo(error.streamId);
                } else if (error.indexOf("data_channel_error") != -1) {
                    errorMessage = "There was a error during data channel communication";
                } else if (error.indexOf("ScreenSharePermissionDenied") != -1) {
                    errorMessage = "ScreenSharePermissionDenied";

                    $(`.host-video-player`).css({ 'transform': 'scaleX(-1)', '-webkit-transform': 'scaleX(-1)' });
                    $(`.host-video-player`).removeClass('force-hidden');
                    $(`.screen-share-active-screen`).addClass('force-hidden');
                    if (isCameraOff) {
                        turnOnLocalCamera();
                    }
                    sendNotificationEvent('SCREEN_SHARE_STOPPED');

                    var screenShareBtnId = document.getElementById("screen-share-btn");
                    screenShareBtnId.innerHTML = "Present now";
                    screenShareFlag = false;

                }

                console.log(`\n errorMessage > ${errorMessage}`);
            }
        });
}