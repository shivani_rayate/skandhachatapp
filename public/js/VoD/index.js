// for formatting bytes to readable file size
function formatBytes(x) {
    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let l = 0, n = parseInt(x, 10) || 0;

    while (n >= 1024 && ++l) {
        n = n / 1024;
    }
    //include a decimal point and a tenths-place digit if presenting 
    //less than ten of KB or greater units
    return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
}

// VoD  Record Listing 

$.get('/v1/video/getVodList',
    function (data, status) {
        if (data.status === 200) {
            destroyRows();
            var array = data.data
            var options_table = '';
            array.forEach(function (element, i) {
                var vodId = element.vodId ? element.vodId : '';
                var streamName = element.streamName ? element.streamName : '';
                var fileSize = element.fileSize ? formatBytes(element.fileSize) : '';
                var modifiedOn = element.creationDate ? moment(element.creationDate).format('lll') : '';
                var filePath = element.filePath ? element.filePath : '';
                options_table += `<tr class="vod-row" id=${vodId}>
            <td class="">${(i + 1)}</td>
            <td class="streamName">${streamName}</td>
            <td class="fileSize">${fileSize}</td>
            <td class="modified_on">${modifiedOn}</td>
            <td class="action-td"> <div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right">
            <a href="#" class="dropdown-item vodplayButton" data-fileName=${filePath}>Play</a>
            <a href="#" class="dropdown-item vodDeleteButton" data-fileName=${filePath}>Delete</a>

            </td>`

                if (i === (array.length - 1)) {
                    $('#list_all_vod_tbody').append(options_table)
                    reInitializeDataTable()
                }

            })
        }
    })



function destroyRows() {
    $('#list_all_vod_tbody').empty()
    $('#list_all_vod_table').DataTable().rows().remove();
    $("#list_all_vod_table").DataTable().destroy()
}


function reInitializeDataTable() {
    $("#list_all_vod_table").DataTable().destroy()
    list_all_vod_table = $('#list_all_vod_table').DataTable({
        "columnDefs": [
            { "width": "40%", "targets": 1 }
        ]
    })
}

$(document).on('click', '.vodplayButton', function (e) {
    const filePath = $(this).attr('data-fileName');
    const url = `https://ant-media.skandha.tv:5443/WebRTCAppEE/${filePath}`
    var video = document.getElementById('vodplayer');
    video.src = url;
    video.play();
    $('#vodVideo').modal('show');
});

$(document).on('click', '.stopVideo', function (e) {
    var video = document.getElementById('vodplayer');
    video.pause();
    video.currentTime = 0;
});


// VoD delete


$(document).on("click", ".vodDeleteButton", function (e) {

    var vodId = $(this).parents(".vod-row").attr('id')

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {
            $.ajax({
                url: `/v1/video/vodDeleteVideo/${vodId}`,
                type: 'DELETE',
                headers: {
                    'Content-Type': "application/json",
                },
                'success': function (data, textStatus, request) {
                    if (data.status === 200) {
                        toastr.success('VoD deleted Successfully.');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    } else {
                        toastr.error('VoD delete failed');
                    }
                },
                'error': function (request, textStatus, errorThrown) {
                    console.log("ERR " + errorThrown)
                }

            })
        }
    })
    e.preventDefault()
})

