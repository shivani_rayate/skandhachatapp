const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
const AWS = require('aws-sdk');

global.fetch = require('node-fetch');
// ####################################################
global.navigator = () => null;  // VIMP SOLUTION on >>>  navigator not defined error
// ####################################################
const awsConfig = require('../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const poolData = {
    UserPoolId: awsConfig.COGNITO.UserPoolId, // Your user pool id here    
    ClientId: awsConfig.COGNITO.ClientId // Your client id here
};

const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);


// Login
exports.Login = (paramsReq) => {
    console.log('login details in cognito file >>>>>> ' + JSON.stringify(paramsReq))

    return new Promise((resolve, reject) => {
        var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
            Username: paramsReq.username,
            Password: paramsReq.password,
        });
        console.log('authenticationDetails >>>>>> ' + JSON.stringify(authenticationDetails))

        var userData = {
            Username: paramsReq.username,
            Pool: userPool
        };
        console.log('userData >> >>  >>>>>> ' + JSON.stringify(userData))

        var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                //console.log('result >>>  + ' + JSON.stringify(result));
                // console.log('access token + ' + result.getAccessToken().getJwtToken());
                // console.log('id token + ' + result.getIdToken().getJwtToken());
                // console.log('refresh token + ' + result.getRefreshToken().getToken());
                let obj = {};
                obj.status = 200;
                obj.message = 'User Authentication Successed';
                obj.data = result;
                resolve(obj)
            },
            onFailure: function (err) {
                console.log(`Login > onFailure > ` + err);
                if (err.message === `Only radix 2, 4, 8, 16, 32 are supported`) {
                    console.log(`Login > RADIX BLOCK `)
                    err.message = `Incorrect username or password.`
                }
                let obj = {};
                obj.status = 403;
                obj.message = err.message;
                obj.data = null;
                resolve(obj)
            },
        });
    })
}


// GetUsers
exports.getUsers = () => {

    var params = {
        UserPoolId: awsConfig.COGNITO.UserPoolId, // Your user pool id here 
        AttributesToGet: [
            'name'
        ],
    };

    return new Promise((resolve, reject) => {
        cognitoidentityserviceprovider.listUsers(params, (err, data) => {
            if (err) {
                console.log(err);
                reject(err)
            }
            else {
                //console.log("getUsers data from cognito >> ", JSON.stringify(data));
                resolve(data)
            }
        })
    })
};