const express = require('express');
const router = express.Router();
const path = require('path');
const cookieParser = require('cookie-parser');
const chalk = require('chalk');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const helmet = require('helmet');
const cors = require('cors');
const compression = require('compression');

const mongoConfig = require('./config/components/mongo');
var index = require('./routes/index');
var video = require('./routes/video');
var cognito = require('./routes/cognito');

const app = express();
mongoConfig.bootstrap();

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3006);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');



// helmet for security purpose
app.use(helmet());

// CORS - To hanlde cross origin requests
app.use(cors());

// Parsing the body of the http
app.use((req, res, next) => {
    bodyParser.json({
        limit: '26000mb',
        verify: (req, res, buf, encoding) => {
            req.rawBody = buf.toString();
        }
    })(req, res, err => {
        if (err) {
            res.status(400).send('Bad body Request');
            return;
        }
        next();
    });
});

// to support URL-encoded bodies
app.use(bodyParser.urlencoded({
    limit: '26000mb',
    extended: true
}));

app.use(cookieParser());

//compress all responses to Make App Faster
app.use(compression());

app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'live-chat-session-parameter',
    resave: true,
    saveUninitialized: true
}));


app.use('/v1', router);
app.use('/', index);

app.use((req, res, next) => {
    if (req.session.email == null) {
        // if user is not logged-in redirect back to login page //
        res.redirect('/');
    } else {
        next();
    }
});


app.use('/v1/video', video);
app.use('/v1/cognito', cognito);


// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    console.log(`ERRR APPP USE!! ${err}`);
    next(err);
});


// error handler
app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
    console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
    console.log('  Press CTRL-C to stop\n');
  });

mongoose.set('debug', true) // for dev only

module.exports = app;