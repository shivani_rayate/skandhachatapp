var express = require('express');
var router = express.Router();

const cognito = require('../app/utilities/cognito');

// Get All Users
router.get('/getUsers', (req, res, next) => {
    console.log('\n /getUsers called in route');

    cognito.getUsers().then((users) => {
        res.json({
            status: 200,
            message: 'Users fetched successfully',
            data: users,
        });
    });

});


module.exports = router;