const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
const cognito = require('../app/utilities/cognito');
const awsConfig = require('../config/aws_config')['development'];

router.get('/', function (req, res, next) {

  let email = req.session ? (req.session.email ? req.session.email : null) : null;
  var meetingCode = req.query.meetingCode ? req.query.meetingCode : null;

  if (email) {
    if (meetingCode) {
      //res.redirect(`http://localhost:3000/v1/video/conferenceCall?meetingCode=${meetingCode}`)
      res.redirect(`https://chatroom.skandha.tv/v1/video/conferenceCall?meetingCode=${meetingCode}`)
    } else {
      res.redirect('/v1/video/createMeeting');
    }
  } else {
    res.render('login');
  }

});


router.get('/temp', function (req, res, next) {
  res.render('temp');
});


// Login (Cognito)
// router.post('/login', (req, res, next) => {
//   console.log("LOGIN POST called")
//   let params = {
//     username: req.body.username,
//     password: req.body.password
//   }
//   console.log("login data >>" + JSON.stringify(params))
//   cognito.Login(params).then((response) => {
//     if (response.status === 200) {
//       console.log('user sign in response >> ' + JSON.stringify(response));
//       //set user details in sesssion
//       req.session.userData = response.data;
//       req.session.email = response.data.idToken.payload.email;
//       req.session.name = response.data.idToken.payload.name;
//       res.json({
//         status: 200,
//         message: 'Login successful'
//       });
//     } else {
//       res.json({
//         status: 403,
//         message: response.message,
//         data: response.data,
//       })
//     }
//   }).catch((err) => {
//     console.log(`error IN USER sign in : ${err}`);
//     res.json({
//       status: 500,
//       message: 'Oops ! Some error occured, please try again later.',
//       data: null,
//     });
//   });
// });


// Login (Testing)
router.post('/login', (req, res, next) => {
  console.log("\nLOGIN POST called")
  let params = {
    username: req.body.username,
    password: req.body.password
  }

  if (params.username && params.password) {

    // getting substring from email id (before @)
    const name = (params.username).substr(0, (params.username).indexOf('@'));

    //set user details in sesssion
    req.session.email = params.username;
    req.session.name = name;
    res.json({
      status: 200,
      message: 'Login successful'
    });
  } else {
    res.json({
      status: 403,
      message: "Invalid credentials",
      data: null,
    })
  }

});


//Logout
router.get('/logout', function (req, res) {
  req.session.destroy(function (err) {
    if (err) {
      res.redirect('/');
    } else {
      req.session = null;
      console.log("Logout Success " + JSON.stringify(req.session) + " ");
      res.redirect('/');
    }
  });
});


module.exports = router;