const express = require('express');
const router = express.Router();
var awsSesMail = require('aws-ses-mail');
var sesMail = new awsSesMail();
const axios = require('axios');

const awsConfig = require('../config/aws_config')['development'];

const API = require('../config/api')['API'];
//const antMediaBaseURL_dev  = API.base.antMediaBaseURL_dev 
const antMediaBaseURL = API.base.antMediaBaseURL_prod;


const Meeting = require("../workers/conferenceMeetings");
const meetingObj = new Meeting.MeetingClass();


router.get('/conferenceCall', function (req, res, next) {

    const email = req.session.email ? req.session.email : null;
    const name = req.session.name ? req.session.name : null;

    res.render('conferenceCall/conferenceCall', { email: email, name: name });
});

router.get('/createMeeting', function (req, res, next) {

    const email = req.session.email ? req.session.email : null;
    const name = req.session.name ? req.session.name : null;

    res.render('conferenceCall/createMeeting', { email: email, name: name });
});

router.get('/VoD', function (req, res, next) {

    const email = req.session.email ? req.session.email : null;
    const name = req.session.name ? req.session.name : null;

    res.render('VoD/index', { email: email, name: name });
});


// Get VoD Listing
router.get('/getVodList', (req, res, next) => {

    axios.get(`${antMediaBaseURL}/v2/vods/list/0/20`)
        .then(function (response) {
            res.json({
                status: 200,
                message: 'VoD Record Listing',
                data: response.data,
            })
        })
        .catch(function (error) {
            console.log(error);
            res.json({
                status: 500,
                message: 'Oops ! Some error occured, please try again later.',
                data: null,
            });
        });
});

// delete VoD 
router.delete('/vodDeleteVideo/:vodId',(req, res, next) =>{
    const vodId = req.params.vodId
    axios.delete(`${antMediaBaseURL}/v2/vods/${vodId}`)
     .then(function(response){
        ////console.log(response);
       res.json({
             status: 200,
             message: 'VoD Record deleted',
             data: response.data,
         })
    })
   .catch(function (error) {
        console.log(error);
        res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
        });
    });
});

// Meeting data save in mongodb
router.post('/saveMeeting', (req, res, next) => {
    console.log("saveMeeting POST called")

    var params = {
        name: req.body.meetingName,
        host: {
            email: req.session.email,
            name: req.session.name,
        },
    }
    meetingObj.save(params).then((response) => {
        console.log('Meeting saved successfuly');
        res.json({
            status: 200,
            message: 'Meeting saved successfuly',
            data: response,
        })
    }).catch((err) => {
        console.log(`error IN saveMeeting : ${err}`);
        res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
        });
    });
})


// addParticipant
router.post('/addParticipant', (req, res, next) => {

    console.log("addParticipant POST called in routes ");

    var params = {
        meetingCode: req.body.meetingCode,
        streamId: req.body.streamId,
        email: req.session.email,
        name: req.session.name,
    }

    meetingObj.addParticipant(params).then((response) => {
        console.log('participant added successfully');
        res.json({
            status: 200,
            data: response,
            message: 'Participant added successfully',
        });
    }).catch((err) => {
        console.log(`error IN addParticipant : ${err}`);
        res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
        });
    });

})




// send meeting invitation meeting link
router.post('/sendInvitationMail', function (req, res, next) {

    console.log('send email post called')

    const params = {
        selected_peoples: req.body.selected_peoples,
        roomName: req.body.roomName,
        meetingCode: req.body.meetingCode,
    }
    sesMail.setConfig(awsConfig.sesConfig);
    var options = {
        from: 'pshinde@skandha.in',
        to: params.selected_peoples,
        subject: 'LiveChat',
        content: `<html><head></head><body><strong>You have been invited to the following event.</strong><div><h4>${params.roomName}</h4></div><div><p>Organizer : ${req.session.email}</p></div><div><p>Joining Info : Join with Skandha Chatroom</p></div><div><p>https://chatroom.skandha.tv/?meetingCode=${params.meetingCode}</p></div> </body></html>`
    };
    sesMail.sendEmail(options, function(data){
        console.log(data);
        res.json({
            status: 200,
            message: 'Invite sent',
            data: null,
        });
    });

});

// get meeting host
router.get('/getMeeting/:meetingCode', (req, res, next) => {

    console.log("getMeeting API called");

    var params = {
        meetingCode: req.params.meetingCode
    }
    meetingObj.findOne(params).then((response) => {
        res.json({
            status: 200,
            message: 'got meeting successfully',
            data: response,
        })
    }).catch((err) => {
        console.log(`error IN getMeeting : ${err}`);
        res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
        });
    });

})

// get Meeting Participant
router.post('/getParticipant', (req, res, next) => {
    console.log('getParticipant post called')

    var params = {
        streamId: req.body.streamId,
        meetingName: req.body.meetingName
    }

    meetingObj.getParticipant(params)
        .then((_response) => {
            console.log('meeting participant got successfully');
            res.json({
                status: 200,
                data: _response,
                message: 'meeting participant got successfully',
            })
        })
        .catch((error) => {
            res.json({
                status: 500,
                message: 'Oops looks like something went wrong, please try again later',
                data: null,
            });
        })
})

// VoD record
router.post('/VoDRecord', (req, res, next) => {

    var params = {
        streamId: req.body.streamId,
        recordingStatus: req.body.recordingStatus
    }

    axios.put(
        `${antMediaBaseURL}/v2/broadcasts/${params.streamId}/recording/${params.recordingStatus}`,
        {}
    )
        .then(function (response) {
            console.log(response);
            res.json({
                status: 200,
                message: 'Record Started',
                data: response.data,
            })
        })
        .catch(function (error) {
            console.log(error);
            res.json({
                status: 500,
                message: 'Oops ! Some error occured, please try again later.',
                data: null,
            });
        });
})


module.exports = router;