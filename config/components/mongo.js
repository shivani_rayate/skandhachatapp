const mongoose = require('mongoose');
const config = require('../../config/config.json');
const constants = require('../../config/constant');
const { DATABASE_MONGO_PREFIX } = constants;

mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);
// deprecation ERROR Fixes
module.exports = {
  bootstrap: () => {
    const mongodb = config.mongodb["development"];
    //const connectionString = `${DATABASE_MONGO_PREFIX}${mongodb.username}:${mongodb.password}@${mongodb.host}:${mongodb.port}/skandhaChat?authSource=admin`;
    const connectionString = `${DATABASE_MONGO_PREFIX}localhost:27017/skandhaChat`;
    console.log(`connectionString: ${connectionString}`);

    mongoose.connect(connectionString, (err, db) => {
      if (err) {
        console.error(err);
        throw new Error('Unable to connect MongoDB');
      }
      console.log(`Connected to MongoDB! ${db}`);
    });
    // TODO: Validate DB Address
  },
  config: null,
};